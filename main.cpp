#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string>
#include <sstream>
#include <stdexcept>

void parseString(char **argv, int index, int &read, int &save);
void parseCommandLine(int argc, char **argv, int &read, int &save);
void help(const char *argv0);
void fileError(const char *filename);

class Graph
{
public:	
	void setRandom();
	void readFromFile(std::ifstream &file);
	void print(std::ostream &output);
	void saveInFile(std::ofstream &file);
		
private:
	struct Vertex
	{
		char label;
		Vertex() {}
		Vertex(char c) : label(c) {}
	};
	
	struct AdjacencyMatrix
	{
		size_t verticesNumber;
		std::vector<bool> data;
		
		void create(size_t vn)
		{
			verticesNumber = vn;
			data.resize(vn*vn);
			for(size_t i=0; i<data.size(); ++i)
				data[i] = 0;
		}
			
		size_t getIndex(size_t x, size_t y)
		{
			return x + y*verticesNumber;
		}
		
		bool operator()(size_t x, size_t y)
		{
			return data[getIndex(x, y)];
		}
	};
	
	std::vector<Vertex> vertices;
	AdjacencyMatrix adjacencyMatrix;

	void addEdge(size_t v1, size_t v2);
};


int main(int argc, char **argv)
{
	int read=0, save=0; // will be indices of filenames in argv (or still 0 if filenames are not given)
	parseCommandLine(argc, argv, read, save);
	Graph graph;
	if(read)
	{
		std::ifstream file(argv[read]);
		if(!file.is_open())
			fileError(argv[read]);
		graph.readFromFile(file);
		file.close();
	}
	else
		graph.setRandom();
	graph.print(std::cout);
	if(save)
	{
		std::ofstream file(argv[save]);
		if(!file.is_open())
			fileError(argv[read]);
		graph.saveInFile(file);
		file.close();
	}
	return 0;
}


void parseString(char **argv, int index, int &read, int &save)
{
	if(std::strcmp(argv[index], "-r") == 0)
		read = index+1;
	else if(std::strcmp(argv[index], "-s") == 0)
		save = index+1;
	else
		help(argv[0]);	
}

void parseCommandLine(int argc, char **argv, int &read, int &save)
{
	switch(argc)
	{
		case 3:
			parseString(argv, 1, read, save);
			break;
		case 5:
			parseString(argv, 1, read, save);
			parseString(argv, 3, read, save);
			break;
		default:
			help(argv[0]);
	}
}

void help(const char *argv0)
{
	std::cout << "Usage: " << argv0 << " <option1> <filename1> (optionally <option2> <filename2>)\n"
	             "Options (1 or 2 are accepted):\n"
	             "-r : graph data will be read from file (filename - after -r)\n"
	             "     if there is no -r, graph data will be chosen at random\n"
	             "-s : graph data will be saved in file (filename - after -s)\n"
	             "     if there is no -s, nothing will be saved.\n";
	std::exit(1);
}

void fileError(const char *filename)
{
	std::cerr << "Could not open file " << filename << "\n";
	std::exit(2);
}


void Graph::setRandom()
{
	std::srand(std::time(0));
	size_t size = std::rand();
	size = size%6 + 4; // number from 4 to 9
	vertices.resize(size);
	for(auto &it : vertices)
		it.label = rand()%('z'-'a') + 'a';
	adjacencyMatrix.create(size);
	for(size_t i=0; i<adjacencyMatrix.data.size(); ++i)
		adjacencyMatrix.data[i] = std::rand()%2;
}

void Graph::readFromFile(std::ifstream &file)
{
#define READ(x) file >> string; if(x) throw std::runtime_error("Incorrect data passed to Graph::readFromFile\n");
	std::string string;
	READ(!file || string != "<Graph>")
	READ(!file || string != "<vertices>")	
	for(;;)
	{
		READ(!file)
		if(string.length() == 1)
			vertices.emplace_back(string[0]);
		else if(string == "</vertices>")
			break;
		else
			throw std::runtime_error("Incorrect data passed to Graph::readFromFile\n");
	}
	adjacencyMatrix.create(vertices.size());
	READ(!file || string != "<edges>")
	for(;;)
	{
		size_t v1, v2;
		size_t control=0;
		READ(!file)
		if(string == "</edges>")
			break;
		v1 = std::stoul(string, &control);
		if(control != string.length())
			throw std::runtime_error("Incorrect data passed to Graph::readFromFile\n");
		READ(!file)
		control = 0;
		v2 = std::stoul(string, &control);
		if(control != string.length())
			throw std::runtime_error("Incorrect data passed to Graph::readFromFile\n");
		addEdge(v1, v2);
	}
	READ(!file || string != "</Graph>")
#undef READ
}

void Graph::print(std::ostream &output)
{
	output << "Graph:\n";
	for(size_t i=0; i<adjacencyMatrix.verticesNumber; ++i) // for each row
	{
		for(size_t j=0; j<adjacencyMatrix.verticesNumber; ++j) // for each column
		{
			if(adjacencyMatrix(j,i))
				output << vertices[i].label << "(" << i << ") ->- " << vertices[j].label << "(" << j << ")\n";
		}		
	}
}

void Graph::saveInFile(std::ofstream &file)
{
	file << "<Graph>\n  <vertices>\n";
	for(const auto &it : vertices)
		file << "    " << it.label << "\n";
	file << "  </vertices>\n  <edges>\n";
	for(size_t i=0; i<adjacencyMatrix.verticesNumber; ++i) // for each row
	{
		for(size_t j=0; j<adjacencyMatrix.verticesNumber; ++j) // for each column
		{
			if(adjacencyMatrix(j,i))
				file << "    " << i << " " << j << "\n";
		}		
	}
	file << "  </edges>\n</Graph>\n";
}

void Graph::addEdge(size_t first, size_t second)
{
    if(first >= vertices.size() || second >= vertices.size()) // >=, not = because of 0-based indexing
        throw std::runtime_error("Cannot add edge - vertex number exceeds graph size.");
    size_t index = adjacencyMatrix.getIndex(second, first); // edges are directed from vertex at row index (y) to vertex at column index (x)
    if(adjacencyMatrix.data[index])
    {
        std::cerr << "Warning - ignored request to add second edge between vertices: "
                  << first << ", " << second << "\n";
        return;
    }
    adjacencyMatrix.data[index] = 1;
}
