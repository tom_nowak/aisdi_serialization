CC = g++
OPT = -Wall -Wextra -pedantic -std=c++11 -O3

Serialization: main.cpp Makefile
	$(CC) $(OPT) main.cpp -o Serialization
