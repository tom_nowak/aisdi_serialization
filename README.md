# Tomasz Nowak

Prosty program do losowej generacji i serializacji skierowanego grafu z etykietami na wierchołkach.

# Uproszczenia:

- Graf nie zapewnia żadnej sensownej funkcjonalności oprócz wypisania wszystkich krawędzi.
- Etykieta to pojedynczy znak (łatwa generacja losowa), nie ma to wpływu na ogólną ideę serializacji.
- Jest tylko jeden graf (serializacja nieznanej liczby numerowanych grafów - w klasie Graph może być pole statyczne - licznik instancji, w konstruktorze każdemu grafowi jest przypisywany identyfikator).

#Uruchamianie programu

Argumenty: <opcja> <nazwa pliku> (opcjonalnie jeszcze raz <opcja> <nazwa pliku>).

Opcje:

-r (read) - program odczyta dane z pliku

-s (save) - program zapisze dane do pliku

Możliwości:

- tylko -r: program odczyta graf z pliku i go wyświetli
- tylko -s: graf zostanie wygenerowany losowo, wyświetlony i zapisany w pliku
- -r i -s: graf zostanie przepisany z pliku, którego nazwa jest za -r, do pliku, którego nazwa jest za -s (ale wcześniej zostanie normalnie utworzony w pamięci i wyświetlony; nie przepisuję zawartości pliku, tylko ją odtwarzam, żeby przetestować serializację).
